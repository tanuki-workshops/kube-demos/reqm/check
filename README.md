# Check

## About

> 🚧 wip

## How to use it?

```yaml
stages:
  - 📝requirements
  
include:
  - project: 'tanuki-workshops/kube-demos/reqm/check'
    file: 'requirements.gitlab-ci.yml'

#-------------------------------
# Requirements management
#-------------------------------
📝:requirements:management:
  stage: 📝requirements
  extends: .requirements:management
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID
  variables:
    # consideration of requirements
    REQUIREMENTS: >
      1:
        title: The user should be able to create a blue environment 🔵
        status: passed
      2:
        title: The user should be able to create a green environment 🟢
        status: passed
      3:
        title: The user should be able to create a green service 🟢
        status: failed
      4:
        title: The user should be able to create a blue service 🔵
        status: failed 
      5:
        title: The user should be able to switch to the green service 🟢
        status: failed     
      6:
        title: The user should be able to switch to the blue service 🔵
        status: passed  
```


