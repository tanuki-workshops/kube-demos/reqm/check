#!/usr/bin/env node

const fs = require("fs")
const yaml = require("js-yaml")

let requirements = process.env.REQUIREMENTS

console.log(requirements)

yaml.safeLoadAll(requirements, reqs => {
  //console.log(reqs)
  let report = new Object()
  for(requirementId in reqs) {
    report[requirementId]=reqs[requirementId].status
  }
  console.log(report)
  fs.writeFileSync("./tmp/requirements.json", JSON.stringify(report, null, 2))
  
})
